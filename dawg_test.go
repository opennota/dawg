// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package dawg

import (
	"bytes"
	"image/png"
	"os"
	"os/exec"
	"strings"
	"testing"
)

func TestDAWG(t *testing.T) {
	words := []string{
		"cat",
		"catnip",
		"zcatnip",
	}

	var d DAWG
	for _, w := range words {
		d.Insert(w)
	}
	d.Finish()

	if got := d.NodeCount(); got != 11 {
		t.Errorf("want node count %d, got %d", 11, got)
	}
	if got := d.EdgeCount(); got != 11 {
		t.Errorf("want edge count %d, got %d", 11, got)
	}

	for _, w := range words {
		if !d.Lookup(w) {
			t.Errorf("want DAWG to contain %q", w)
		}
	}

	for _, w := range []string{
		"",
		"c",
		"ca",
		"cats",
		"catni",
		"catnipz",
		"z",
		"zc",
		"zcat",
		"zcatn",
		"zcatni",
		"zcatnipz",
		"at",
		"t",
		"p",
		"ip",
		"nip",
		"tnip",
		"atnip",
		"fat",
	} {
		if d.Lookup(w) {
			t.Errorf("want DAWG to not contain %q", w)
		}
	}
}

func TestFromReader(t *testing.T) {
	words := []string{
		"pity",
		"cities",
		"pities",
		"city",
	}
	r := strings.NewReader(strings.Join(words, "\n"))
	d, err := FromReader(r)
	if err != nil {
		t.Fatalf("want nil error, got %v", err)
	}

	if got := d.NodeCount(); got != 7 {
		t.Errorf("want node count %d, got %d", 7, got)
	}
	if got := d.EdgeCount(); got != 8 {
		t.Errorf("want edge count %d, got %d", 8, got)
	}

	for _, w := range words {
		if !d.Lookup(w) {
			t.Errorf("want DAWG to contain %q", w)
		}
	}
}

func TestInsertPanic(t *testing.T) {
	var d DAWG
	d.Insert("donna")

	const want = "words must be inserted in alphabetical order"
	defer func() {
		if r := recover(); r == nil {
			t.Error("want panic, got none")
		} else if r.(string) != want {
			t.Errorf("want %q panic, got %q", want, r)
		}
	}()
	d.Insert("bella")
}

func TestUnicode(t *testing.T) {
	words := []string{
		"кот",
		"котлета",
		"плотина",
		"скот",
		"скотина",
	}

	var d DAWG
	for _, w := range words {
		d.Insert(w)
	}
	d.Finish()

	if got := d.NodeCount(); got != 33 {
		t.Errorf("want node count %d, got %d", 33, got)
	}
	if got := d.EdgeCount(); got != 34 {
		t.Errorf("want edge count %d, got %d", 34, got)
	}

	for _, w := range words {
		if !d.Lookup(w) {
			t.Errorf("want DAWG to contain %q", w)
		}
	}
}

func TestPrintDot(t *testing.T) {
	if _, err := exec.LookPath("dot"); err != nil {
		t.Skip("dot not found")
	}

	words := []string{
		"cat",
		"catnip",
		"zcatnip",
	}

	var d DAWG
	for _, w := range words {
		d.Insert(w)
	}
	d.Finish()

	var buf bytes.Buffer
	if err := d.PrintDOT(&buf); err != nil {
		t.Fatalf("want no errors, got %v", err)
	}

	var outBuf bytes.Buffer
	cmd := exec.Command("dot", "-Tpng")
	cmd.Stdin = &buf
	cmd.Stdout = &outBuf
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		t.Fatal(err)
	}

	if _, err := png.DecodeConfig(&outBuf); err != nil {
		t.Errorf("want a PNG file, got something else")
	}
}
