dawg [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![GoDoc](http://godoc.org/gitlab.com/opennota/dawg?status.svg)](http://godoc.org/gitlab.com/opennota/dawg) [![Pipeline status](https://gitlab.com/opennota/dawg/badges/master/pipeline.svg)](https://gitlab.com/opennota/dawg/commits/master)
====

Package dawg provides functions for constructing and querying Directed Acyclic Word Graphs (a.k.a Minimal Acyclic Finite-State Automata).

## Install

    go get -u gitlab.com/opennota/dawg
